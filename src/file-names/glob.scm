;; Copyright (C) 2019 Brandon Invergo <brandon@invergo.net>

;; This library is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

(define-module (file-names glob)
  #:use-module (oop goops)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 ftw)
  #:use-module (file-names)
  #:export (glob))

;;; Just a quick helper function.  I don't think these necessarily
;;; should be provided for all file-types returned by lstat.
(define-method (dir? base name)
  (let* ((d (add-prefix base (string->file-name name)))
         (d-stat (fn-lstat d)))
    (eq? (stat:type d-stat) 'directory)))

(define (make-wildcard-regexp pat . flags)
  "Construct a proper regular expression from shell-style wildcard
pattern PAT.  Any additional FLAGS are passed on to the make-regexp
procedure."
  (let* ((rx-pat (fold
                  (lambda (pattern prev)
                    (regexp-substitute/global #f (car pattern) prev
                                              'pre (cdr pattern) 'post))
                  pat '(("\\." . "\\.")
                        ("\\*" . ".*")
                        ("\\?" . "."))))
         (rx-final (string-join `("^" ,rx-pat "$") "")))
    (apply make-regexp rx-final flags)))

(define (compile-regexp pat match-style case-sensitive)
  "Compile a new regular expression from pattern PAT depending on
MATCH-STYLE (either wildcard or regexp).  Case-sensitivity is
determined by boolean CASE-SENSITIVE."
  (case match-style
    ((wildcard)
     (if case-sensitive
         (make-wildcard-regexp pat regexp/icase)
         (make-wildcard-regexp pat)))
    ((regexp)
     (if case-sensitive?
         (make-regexp (string-join `("^" ,pat "$") "") regexp/icase)
         (make-regexp (string-join `("^" ,pat "$") ""))))))

(define (dots? s)
  (or (string=? s ".")
      (string=? s "..")))

(define (glob-dir dir pat-list match-style)
  "Recursively match files in directory DIR using the list of patterns
PAT-LIST according to MATCH-STYLE (wildcard or regexp).  PAT-LIST
contains a separate pattern for subsequent levels in the file-system
hierarchy.  The first item in the list is applied to files in DIR, the
second is applied to files in sub-directories of DIR, the third is
applied to sub-sub-directories, etc."
  (match pat-list
    (() '())
    (("**")
     ;; For '**' without additional wildcard patterns return
     ;; everything in DIR and all its sub-directories
     (concatenate
      (list (glob-dir dir '("*") match-style)
            (concatenate (map
                          (lambda (d-match)
                            (glob-dir (push dir d-match) pat-list match-style))
                          (scandir (file-name->string dir)
                                   (lambda (d)
                                     (and (not (dots? d))
                                          (dir? dir d)))))))))
    (("**" pat ..1)
     ;; For '**' with additional wildcard patterns
     ;; (e.g. '**/*.scm'), return the same set of patterns
     ;; (including '**') on all the sub-directories as well as
     ;; matching the remaining patterns (e.g. '*.scm') against
     ;; the current directory.
     (concatenate
      (list (glob-dir dir (cdr pat-list) match-style)
            (concatenate (map
                          (lambda (d-match)
                            (glob-dir (push dir d-match) pat-list match-style))
                          (scandir (file-name->string dir)
                                   (lambda (d)
                                     (and (not (dots? d))
                                          (dir? dir d)))))))))
    ((pat)
     ;; A lone pattern, just match files in the current directory
     (let ((pat-rx (compile-regexp pat match-style (case-sensitive? dir))))
       (map (lambda (x-match)
              (push dir x-match))
            (scandir (file-name->string dir)
                     (lambda (x)
                       (and (not (dots? x))
                            (regexp-exec pat-rx x)))))))
    ((pat1 pat2 ..1)
     ;; A directory-matching pattern.  Match sub-directories in DIR
     ;; and then match the remaining patterns in sub-sub-directories.
     (let ((pat-rx (compile-regexp pat1 match-style (case-sensitive? dir))))
       (concatenate (map
                     (lambda (d-match)
                       (glob-dir (push dir d-match) (cdr pat-list) match-style))
                     (scandir (file-name->string dir)
                              (lambda (d)
                                (and
                                 (not (dots? d))
                                 (regexp-exec pat-rx d)
                                 (dir? dir d))))))))))

(define-method (glob (dir <file-name>) (pattern <file-name>) match-style)
  "Return a list of <file-name> objects under the directory
represented by <file-name> object DIR that match the file-name pattern
contained in <file-name> object PATTERN.  The matching is done
according to the symbol in MATCH-STYLE: regexp uses regular
expressions and wildcard does shell-style wildcard matching."
  (let ((rel-pattern (if (fn-absolute-file-name? pattern)
                         (remove-prefix pattern (common-prefix pattern dir))
                         pattern)))
    (glob-dir dir (reverse (drop-right (route rel-pattern) 0)) match-style)))
