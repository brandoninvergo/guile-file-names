;; Copyright (C) 2019 Brandon Invergo <brandon@invergo.net>

;; This library is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

(define-module (file-names)
  #:use-module (oop goops)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:export (<file-name>
            route
            volume
            separator
            case-sensitive?
            make-file-name
            file-name->string
            string->file-name
            absolute-file-name
            canonical-file-name
            add-prefix
            add-prefix!
            common-prefix
            file-name=?
            prefix?
            remove-prefix
            remove-prefix!
            push
            push!
            pop
            pop!
            suffix
            add-suffix
            add-suffix!
            tmp-file-name
            fn-absolute-file-name?
            fn-access
            fn-basename
            fn-chmod
            fn-chown
            fn-copy-file
            fn-delete-file
            fn-dirname
            fn-file-exists?
            fn-link
            fn-lstat
            fn-mkdir
            fn-mknod
            fn-opendir
            fn-readlink
            fn-rename-file
            fn-rmdir
            fn-sendfile
            fn-stat
            fn-symlink
            fn-utime))

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; A word on terminology ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Note that here we when refer to a "file name", we also include the
;; hierarchy of directories that contain the file.  We avoid using the
;; words "pathname" or "path" in accordance with the GNU Coding
;; Standards [1] to avoid confusion with search paths for executables
;; (e.g. the `PATH` environment variable).  Also keep in mind that, at
;; least on a POSIX system, a directory is itself a special kind of
;; file.  Lastly, when we want to refer directly to the "path" of
;; directories that one must traverse to find a file, we call it a
;; "route".
;; [1] http://www.gnu.org/prep/standards/html_node/GNU-Manuals.html#GNU-Manuals,

;;;;;;;;;;;;;;;;;;
;; Design Notes ;;
;;;;;;;;;;;;;;;;;;
;;; Why object oriented?
;;; --------------------
;; I decided to opt for an object oriented implementation.  I had in
;; mind the possibility of specializing the file-name functionality
;; for different use-cases.  In particular, one might want to add new
;; functionality around, e.g. the various file systems created by the
;; kernel Linux (/sys, /proc) or the user-space file-like translators
;; provided by GNU Hurd.
;;
;;; Data structures
;;; ---------------
;; The <file-name> class only has a few slots at present.  VOLUME is a
;; Windows-only thing that represents the drive (e.g. "C:"; although,
;; note to self or anyone reading: it could theoretically be co-opted
;; on POSIX systems to represent remote hosts "myserver:/foo/bar").
;; SEPARATOR stores the file-name separator ("/" on POSIX, "\" on
;; Windows). CS stores a boolean value about whether or not the file
;; name is case-sensitive.
;;
;; The main workhorse of the <file-name> class is the ROUTE slot.  As
;; described above, ROUTE contains the full file-system hierarchy of a
;; file's location.  If a file's location in human-readable string
;; form is (in POSIX notation) /foo/bar/baz/quux.txt, then "quux.txt"
;; is located in directory "baz", which is located in the directory
;; "bar", which is in turn in the directory "foo", which is in the
;; root of the file system.
;;
;; A natural data structure for ROUTE is thus a good, old-fashioned
;; list.  The root of the file system is the empty list ().
;; Directories are added to ROUTE by cons-ing them to the existing
;; route.  So, /foo/bar/baz/quux.txt is
;; (cons "quux.txt" (cons "baz" (cons "bar" (cons "foo" '())))
;; or
;; ("quux.txt" "baz" "bar" "foo")
;;
;; Hang on a second, that's the reverse of what we're used to reading!
;; Why not simply split the string by the file-name separator ("/")
;; into a list and be done with it?  That's true, however we must
;; consider that a) when working with a given file-name, we are most
;; likely to manipulate its more specific parts (that is, the
;; right-most file-name components in the human-readable form; "baz"
;; is more specific than "bar" in our running example); and b) the
;; most inexpensive list manipulations in scheme work on/from the head
;; of the list rather than on the tail.  This is a fancy way of saying
;; that in the initial, naive implementation, I realized that I was
;; reversing the list too often.
;;
;; If a file-name is best represented by a list, then what about a
;; relative file-name?  A relative file-name on its own is severed
;; from the file system; the exact relationship between the location
;; it describes and the root of the file system is unresolved.
;; Therfore, representing a relative route as a list would violate our
;; assignment of the file-system root to the empty list, because all
;; proper lists in Scheme have an empty list in the cdr of their final
;; pair.  If the root of the file system should be represented by the
;; empty list, then a relative file name would naturally be an
;; improper list, in which the cdr of the last pair is undefined.
;;
;; So, the relative route "zyxxy/plugh/fnord.csv" is represented as
;; (cons "fnord.csv" (cons "plugh" (cons "zyxxy" #f)))
;; or
;; ("fnord.csv" "plugh" "zyxxy" . #f)
;;
;; Now, resolving the absolute location of this file is simply a
;; matter of setting the cdr of the last pair to another route.
;; Beautiful!
;;
;; Note that at least at the moment, the value of the last
;; pair's cdr in a relative route is really undefined.  The value is
;; never used, so it doesn't really matter what goes there.  If some
;; meaningful use of that value can be found, I might consider
;; adopting it.
;;
;; Conceptually, this representation of file routes at the most basic
;; of Scheme levels was too good to pass up.  If, in practice, it ends
;; up to be a dog, then I guess it'll have to change.  That would be a
;; shame, though.

(define-class <file-name> ()
  (route #:accessor route #:init-form (list) #:init-keyword #:route)
  (volume #:accessor volume
          #:init-thunk (lambda ()
                         (case (system-file-name-convention)
                           ((windows) #\C)
                           ((posix) #f)))
          #:init-keyword #:volume)
  (separator #:accessor separator
             #:init-thunk (lambda () (string-copy file-name-separator-string))
             #:init-keyword #:separator)
  (cs #:getter case-sensitive? #:init-value #t #:init-keyword #:case-sensitive))

;;; A convenience, to avoid having to use the GOOPS module in your
;;; code.  <file-name> instances created in this manner should have
;;; their ROUTE slot built up through file-name-push/pop/append
;;; operations.
(define* (make-file-name #:key volume absolute separator case-sensitive)
  "Make a new <file-name> instance.  Keyword arguments set the slot
values for the instance: VOLUME sets the drive/volume name, SEPARATOR
sets the file-name separator, and CASE-SENSITIVE sets whether the file
name is (take a guess...) case-sensitive.  Finally, ABSOLUTE takes a
boolean value that is used to initialize the file-name route
appropriately for an absolute or relative route."
  (make <file-name>
    #:volume volume
    #:separator separator
    #:case-sensitive case-sensitive
    #:route (if absolute (list) #f)))

;;; Specialize GOOPS:deep-clone on <file-name> in order to properly
;;; copy the underlying slots that aren't themselves objects.
(define-method (deep-clone (f <file-name>))
  (make <file-name>
    #:volume (volume f)
    #:separator (string-copy (separator f))
    #:cs (case-sensitive? f)
    #:route (list-copy (route f))))

(define-method (fn-absolute-file-name? (f <file-name>))
  (proper-list? (route f)))

(define-method (file-name->string (f <file-name>))
  "Construct a platform-specific string representation of file-name F."
  (if (fn-absolute-file-name? f)
      (format #f "~@[~a:~]~a~a"
              (volume f)
              (separator f)
              (string-join (reverse (route f)) (separator f) 'infix))
      (string-join (reverse (drop-right (route f) 0)) (separator f) 'infix)))

;;; This is probably the main way of creating a new <file-name>
;;; instance.
(define* (string->file-name file-string #:key volume separator case-sensitive
                            #:rest r)
  "Return a new file-name object derived from string representation
FILE-STRING.  If no keywords options are given, the system defaults
are used for volume, separator and case-sensitivity.

Keyword options VOLUME, SEPARATOR and CASE-SENSITIVE can be used to
override system defaults."
  (let* ((sep (if separator
                  (car (string->list separator))
                  (car (string->list file-name-separator-string))))
         (cs (or case-sensitive (and (not (memq #:volume r))
                                     (eq? (system-file-name-convention)
                                          'posix))))
         (vol-split (string-split file-string #\:))
         (vol (if volume
                  volume
                  (if (> (length vol-split) 1)
                      (car (string->list (car vol-split)))
                      #f)))
         (route-str (if (> (length vol-split) 1)
                        (cadr vol-split)
                        file-string))
         (abs (if separator
                  (string-prefix? separator route-str)
                  (absolute-file-name? route-str)))
         (route (reverse (delete "" (string-split route-str sep)))))
    (if (not abs) (set-cdr! (last-pair route) #f))
    (make <file-name> #:absolute abs
          #:separator (if separator separator file-name-separator-string)
          #:route route
          #:case-sensitive cs
          #:volume vol)))

(define-method (display (f <file-name>) port)
  (display (string-append "#<<file-name> " (file-name->string f) ">")) port)

(define-method (write (f <file-name>) port)
  (write (string-append "#<<file-name> " (file-name->string f) ">")) port)

(define-method (add-prefix (f1 <file-name>) (f2 <file-name>))
    "Return a new file name with file name F2 appended to F1.  The new
file name otherwise carries the properties of F1 (separator,
absolute-ness)."
    (let ((f (deep-clone f2)))
      (set-cdr! (last-pair (route f)) (list-copy (route f1)))
      f))
(define-method (add-prefix (f1 <file-name>)) f1)
(define-method (add-prefix) '())
(define-method (add-prefix . args)
    (add-prefix (car args) (apply add-prefix (cdr args))))

(define-method (add-prefix! (f1 <file-name>) (f2 <file-name>))
  "Append file name F2 to F1, modifying F1 in place."
  (let ((new-route (list-copy (route f2))))
    (set-cdr! (last-pair new-route) (route f1))
    (set! (route f1) new-route))
  f1)
(define-method (add-prefix! (f1 <file-name>)) f1)
(define-method (add-prefix!) '())
(define-method (add-prefix! . args)
    (add-prefix! (car args) (apply add-prefix! (cdr args))))

(define-method (pop (f <file-name>))
  "Return the last component of file name F as a string."
  (car (route f)))

(define-method (pop! (f <file-name>))
  "Remove the last component of file name F and return it as a
string."
  (let ((s (car (route f))))
    (set! (route f) (cdr (route f)))
    s))

(define-method (push (f <file-name>) (c <string>))
  "Return a new file name with string component C added to the end of
file name F."
  (let ((f2 (deep-clone f)))
    (set! (route f2) (cons c (route f2)))
    f2))

(define-method (push! (f <file-name>) (c <string>))
  "Append component C to the end of file name F, modifying F in
place."
  (set! (route f) (cons c (route f)))
  f)

;;; A helper procedure.
(define (follow-dots route)
  "Follow and remove '..' entries from ROUTE."
  (match route
    ((".." ..1 d ...)
     (receive (dots rest) (span (lambda (x) (string=? x "..")) route)
       (if (> (length dots) (length rest))
           (error "More '..' than components to remove")
           (follow-dots (drop rest (length dots))))))
    ((d d2 ..1)
     (cons (car route) (follow-dots (cdr route))))
    ((".." . d)
     (error "'..' found at the root of the file name."))
    ((d)
     (list d))
    (#f #f)
    (() (list))))

;;; Another helper procedure
(define (remove-dots f)
  "Remove all '.' and '..' entries from file name F."
  (delete! "." (route f))
  (set! (route f) (follow-dots (route f)))
  f)

(define-method (absolute-file-name (f <file-name>))
  "Return a new file name that represents the canonical, absolute
location of file name F.  All '.' and '..' entries are removed and, if
F is a relative file name, the absolute name is calculated from the
current working directory."
  (if (fn-absolute-file-name? f)
      (remove-dots (deep-clone f))
      (let ((cwd (string->file-name (getcwd))))
        (remove-dots (add-prefix cwd f)))))

(define-method (fn-readlink (f <file-name>))
  (string->file-name (readlink (file-name->string f))))

(define-method (canonical-file-name (f <file-name>))
  "Canonicalize file name F.  All '.' and '..' entries are removed,
all symlinks are resolved and, if F is a relative file name, the
absolute name is calculated from the current working directory."
  (readlink (absolute-file-name f)))

(define-method (fn-dirname (f <file-name>))
  "Return a new file name object that represents the directory part of
file name F (everything but the last component retained).  This
ignores the fact that the last component might itself describe a
directory."
  (let ((f2 (deep-clone f)))
    (set! (route f2) (cdr (route f2)))
    f2))

(define-method (fn-basename (f <file-name>) . rest)
  (if (null? rest)
      (pop f)
      (let* ((name (pop f))
             (ext (car rest))
             (in-name (if (case-sensitive? f)
                          (string-contains name ext)
                          (string-contains-ci name ext))))
        (if in-name
            (substring name 0 in-name)
            name))))

;;; A helper procedure for recursion.
(define (build-common-prefix route1 route2)
  "Recursively build up the common route between ROUTE1 and ROUTE2."
  (let ((tail1 (last-pair route1))
        (tail2 (last-pair route2)))
    (if (not (equal? tail1 tail2))
        #f
        (cond ((build-common-prefix (drop-right route1 1)
                                   (drop-right route2 1)) =>
                (lambda (result)
                  (concatenate (reverse (list tail1 result)))))
              (else tail1)))))

(define-method (common-prefix (f1 <file-name>) (f2 <file-name>))
  "Find the longest route shared by file names F1 and F2 and return it
as a new file name.  If no common prefix exists or if one file name is
absolute while the other one is relative, the return #f."
  (let ((comp? (if (and (case-sensitive? f1) (case-sensitive? f2))
                   string=?
                   string-ci=?))
        (new-route (deep-clone f1)))
    (set! (route new-route)
      (build-common-prefix (route new-route) (route f2)))
    new-route))

(define-method (file-name=? (f1 <file-name>) (f2 <file-name>))
  "Test file names F1 and F2 for equality, accounting for
case-sensitivity of the route components."
  (let ((comp? (if (and (case-sensitive? f1)
                        (case-sensitive? f2))
                   string=?
                   string-ci=?)))
    (if (not (eq? (fn-absolute-file-name? f1)
                  (fn-absolute-file-name? f2)))
        #f
        (and (eq? (case-sensitive? f1) (case-sensitive? f2))
             (string=? (slot-ref f1 'separator) (slot-ref f2 'separator))
             (if (and (fn-absolute-file-name? f1)
                      (fn-absolute-file-name? f2))
                 (list= comp? (route f1) (route f2))
                 (list= comp? (drop-right (route f1) 0)
                        (drop-right (route f2) 0)))))))

(define-method (prefix? (f1 <file-name>) (f2 <file-name>))
  "Test whether F1 is a prefix of F2, i.e. whether F2 is under F1 in
the file-system hierarchy."
  (file-name=? f1 (common-prefix f1 f2)))

(define-method (remove-prefix (f <file-name>) (prefix <file-name>))
  "Return a new relative <file-name> instance with PREFIX removed from
F.  If PREFIX is not a prefix of F, return #f."
  (if (not (prefix? prefix f))
      #f
      (let* ((f2 (deep-clone f)))
        (set! (route f2)
          (list-head (route f2)
                     (- (length (route f2)) (length (route prefix)))))
        (set-cdr! (last-pair (route f2)) #f)
        f2)))

(define-method (remove-prefix! (f <file-name>) (prefix <file-name>))
  "Return a new relative <file-name> instance with PREFIX removed from
F.  If PREFIX is not a prefix of F, return #f."
  (if (not (prefix? prefix f))
      #f
      (begin (set! (route f)
               (list-head (route f)
                          (- (length (route f)) (length (route prefix)))))
             (set-cdr! (last-pair (route f)) #f)
             f)))

(define-method (suffix (f <file-name>))
  "Extract the file suffix, if any, from file name F.  The file
suffix is assumed to be any sequence of characters following the
right-most period character ('.') in the last component of F's route."
  (let* ((name (pop f))
         (dot-index (string-index-right name #\.)))
    (if dot-index (substring name dot-index) "")))

(define-method (add-suffix (f <file-name>) (suffix <string>))
  "Return a new file-name object with SUFFIX added to the last
component of file-name F."
  (let ((f1 (deep-clone f)))
    (set-car! (route f1) (string-append (pop f1) suffix))
    f1))

(define-method (add-suffix! (f <file-name>) (suffix <string>))
  "Add SUFFIX to the last component of file-name F."
  (set-car! (route f) (string-append (pop f) suffix)))

(define (tmp-file-name)
  "Generate a new temporary file name via the tmpnam procedure.  See
the tmpnam documentation for more information."
  (string->file-name (tmpnam)))

;;; Specialize the rest of the primitive Guile file-system procedures
;;; on <file-name> objects.
(define-method (fn-access? (f <file-name>) how)
  (access? (file-name->string f) how))

(define-method (fn-stat (f <file-name>))
  (stat (file-name->string f)))

(define-method (fn-lstat (f <file-name>))
  (lstat (file-name->string f)))

(define-method (fn-chown (f <file-name>) owner group)
  (chown (file-name->string f) owner group))

(define-method (fn-chmod (f <file-name>) mode)
  (chmod (file-name->string f) mode))

;;; This is a bit lazy...
(define-method (fn-utime (f <file-name>) . args)
  (if (null? args)
      (utime (file-name->string f))
      (apply utime (file-name->string f) args)))

(define-method (fn-delete-file (f <file-name>))
  (delete-file (file-name->string f)))

(define-method (fn-copy-file (old-file <file-name>) (new-file <file-name>))
  (copy-file (file-name->string old-file) (file-name->string new-file)))

(define-method (fn-sendfile (out <file-name>) (in <file-name>) count)
  (sendfile (file-name->string out) (file-name->string in) count))
(define-method (fn-sendfile (out <file-name>) (in <file-name>) count offset)
  (sendfile (file-name->string out) (file-name->string in) count offset))

(define-method (fn-rename-file (old-file <file-name>) (new-file <file-name>))
  (rename-file (file-name->string old-file) (file-name->string new-file)))

(define-method (fn-link (old-file <file-name>) (new-file <file-name>))
  (link (file-name->string old-file) (file-name->string new-file)))

(define-method (fn-symlink (old-file <file-name>) (new-file <file-name>))
  (symlink (file-name->string old-file) (file-name->string new-file)))

(define-method (fn-mkdir (f <file-name>))
  (mkdir (file-name->string f)))
(define-method (fn-mkdir (f <file-name>) mode)
  (mkdir (file-name->string f) mode))

(define-method (fn-rmdir (f <file-name>))
  (rmdir (file-name->string f)))

(define-method (fn-opendir (f <file-name>))
  (opendir (file-name->string f)))

(define-method (fn-mknod (f <file-name>) type perms dev)
  (mknod (file-name->string f) type perms dev))

(define-method (fn-file-exists? (f <file-name>))
  (file-exists? (file-name->string f)))

