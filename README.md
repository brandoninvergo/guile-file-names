# guile-file-names

guile-file-names is a module for the [Guile programming
language](https://www.gnu.org/software/guile), a dialect of Scheme.
The module provides functionality for programmatically manipulating
file names.

## About

The module is built around the opinion that the canonical string
representation of file names (e.g. `"/etc/foo/bar"`) is useful for
humans to input and read, but it is a pain to do any non-trivial
manipulations of it (especially if it has been inputted by one of said
humans).  Inevitably, that string ends up getting pulled apart anyway,
so let's start from that point.  The functionality is similar to, but
not based upon, file name modules in other Scheme implementations and
in Common Lisp.  In particular, only POSIX and Windows-style file
names are supported (sorry, MULTICS users!).

The `(file-names)` module is intended to complement [Guile's built-in
file-system
procedures](https://www.gnu.org/software/guile/manual/html_node/File-System.html#File-System).
Accordingly, for each of the relevant file-system procedures, there
exists a variant that accepts file-name objects as arguments instead
of string values in the built-in procedure.

### A word on terminology

Note that here we when refer to a "file name", we also include the
hierarchy of directories that contain the file.  We avoid using the
words "pathname" or "path" in accordance with the [GNU Coding
Standards](http://www.gnu.org/prep/standards/html_node/GNU-Manuals.html#GNU-Manuals),
to avoid confusion with search paths for executables (e.g. the `PATH`
environment variable).  Also keep in mind that, at least on a POSIX
system, a directory is itself a special kind of file.  Lastly, when we
want to refer directly to the "path" of directories that one must
traverse to find a file, we call it a "route".

## Download

Releases are available for download from
http://brandon.invergo.net/software/download/guile-file-names

## Contributing or reporting bugs

Development occurs [on
Gitlab](https://www.gitlab.com/brandoninvergo/guile-file-names).
Contributions, suggestions, and comments are welcome.  Please submit
any bug reports through [the project's Issue
Tracker](https://gitlab.com/brandoninvergo/guile-file-names/issues).

## License

This library is free software: you can redistribute it and/or modify
it under the terms of the [GNU Lesser General Public
License](http://www.gnu.org/licenses/lgpl.html) as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.


