;; Copyright (C) 2019 Brandon Invergo <brandon@invergo.net>

;; This library is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

(define-module (test-file-names)
  #:use-module (oop goops)
  #:use-module (file-names)
  #:use-module (srfi srfi-64))

(test-begin "file names")

(define test-fn-string1 (case (system-file-name-convention)
                          ((posix) "/foo/bar/baz")
                          ((windows) "C:\\foo\\bar\\baz")))
(define test-fn-string1-dir (case (system-file-name-convention)
                              ((posix) "/foo/bar")
                              ((windows) "C:\\foo\\bar")))
(define test-fn-string1-file (case (system-file-name-convention)
                              ((posix) "baz")
                              ((windows) "baz")))
(define test-fn-string1-kw (case (system-file-name-convention)
                             ((windows) "/foo/bar/baz")
                             ((posix) "C:\\foo\\bar\\baz")))
(define test-fn-string2 (case (system-file-name-convention)
                          ((posix) "quux/plugh/xyzzy")
                          ((windows) "quux\\plugh\\xyzzy")))
(define test-fn-string2-dir (case (system-file-name-convention)
                          ((posix) "quux/plugh")
                          ((windows) "quux\\plugh")))
(define test-fn-string3 (case (system-file-name-convention)
                          ((posix) "/foo/bar/../bar/./baz/../../bar/baz")
                          ((windows) "C:\\foo\\bar\\..\\bar\\.\\baz\\..\\..\\bar\\baz")))
(define test-fn-string4 (case (system-file-name-convention)
                          ((posix) "foo/bar/baz")
                          ((windows) "foo\\bar\\baz")))
(define test-fn-string5 (case (system-file-name-convention)
                          ((posix) "/foo/bar/baz.txt")
                          ((windows) "C:\\foo\\bar\\baz.txt")))
(define test-fn-string6 (case (system-file-name-convention)
                          ((posix) "quux/plugh/jelly.txt")
                          ((windows) "quux\\plugh\\jelly.txt")))
(define bogus-dots-fn-string (case (system-file-name-convention)
                               ((posix) "/../foo/bar")
                               ((windows) "\\..\\foo\\bar")))

(define test-fn1 (string->file-name test-fn-string1))
(define test-fn1-dir (string->file-name test-fn-string1-dir))
(define test-fn1-file (string->file-name test-fn-string1-file))
(define test-fn1-kw (string->file-name
                     test-fn-string1-kw
                     #:volume "Z"
                     #:separator (if (eq? (system-file-name-convention)
                                          'posix)
                                     "\\"
                                     "/")
                     #:case-sensitive (eq? (system-file-name-convention)
                                           'windows)))
(define test-fn2 (string->file-name test-fn-string2))
(define test-fn2b (string->file-name test-fn-string2))
(slot-set! test-fn2b 'cs (not (case-sensitive? test-fn2b)))
(define test-fn3 (string->file-name test-fn-string3))
(define test-fn4 (string->file-name test-fn-string4))
(define test-fn5 (string->file-name test-fn-string5))
(define test-fn6 (string->file-name test-fn-string6))
(define bogus-dots-fn (string->file-name bogus-dots-fn-string))

(test-equal "file-name->string" test-fn-string1
            (file-name->string test-fn1))

(test-assert "string->file-name -- route not empty"
  (not (null? (route test-fn1))))

(test-equal "string->file-name -- separator set"
  file-name-separator-string (separator test-fn1))

(test-equal "string->file-name sets non-standard options through
keyword arguments: volume" "Z" (volume test-fn1-kw))

(test-equal "string->file-name sets non-standard options through
keyword arguments: separator"
  (if (eq? (system-file-name-convention) 'posix)
      "\\"
      "/")
  (separator test-fn1-kw))

(test-equal "file-name->string obeys non-standard defaults"
  (if (eq? (system-file-name-convention) 'posix)
      "Z:\\foo\\bar\\baz"
      "Z:/foo/bar/baz")
  (file-name->string test-fn1-kw))

(test-equal "string->file-name sets non-standard options through
keyword arguments: case-sensitive"
  (eq? (system-file-name-convention) 'windows) (case-sensitive? test-fn1-kw))

(if (eq? (system-file-name-convention) 'windows)
    (test-assert "Windows file-names are case-insensitive by default"
      (not (case-sensitive? test-fn1)))
    (test-assert "POSIX file-names are case-sensitive by default"
      (case-sensitive? test-fn1)))

(test-assert "Detect absolute file-name"
  (fn-absolute-file-name? test-fn1))

(test-assert "Detect relative file-name"
  (not (fn-absolute-file-name? test-fn2)))

(test-equal "Adding a prefix gives the right final path."
  "/foo/bar/baz/quux/plugh/xyzzy"
  (file-name->string (add-prefix test-fn1 test-fn2)))

(test-equal "Adding multiple prefixes gives the right final path."
  "/foo/bar/baz/quux/plugh/xyzzy/foo/bar/baz"
  (file-name->string (add-prefix test-fn1 test-fn2 test-fn4)))

(test-assert "adding a prefix inherits the slots of the prefix:
absoluteness"
  (fn-absolute-file-name? (add-prefix test-fn1 test-fn2)))

(test-equal "Adding a prefix inherits the slots of the prefix: separator"
  file-name-separator-string
  (separator (add-prefix test-fn1 test-fn2)))

(test-eq "Adding a prefix inherits the slots of the prefix: case
sensitivity"
  (case-sensitive? test-fn1)
  (case-sensitive? (add-prefix test-fn1 test-fn2b)))

(let ((tmp-fn (deep-clone test-fn1)))
  (add-prefix! tmp-fn test-fn2)
  (test-equal "Destructive adding of prefixes"
    "/foo/bar/baz/quux/plugh/xyzzy"
    (file-name->string tmp-fn)))

(test-equal "Using absolute-file-name to remove dots from an
already-absolute file name"
  test-fn-string1 (file-name->string
                   (absolute-file-name test-fn3)))

(test-error "Trying to resolve double-dots at the root of a file name"
            (absolute-file-name bogus-dots-fn))

(test-equal "Getting the directory part of a file name"
  test-fn-string1-dir (file-name->string (fn-dirname test-fn1)))

(test-equal "Getting the file part of a file name"
  test-fn-string1-file (fn-basename test-fn1))

(test-equal "Getting the file part of a file name without the suffix/extension"
  "baz" (fn-basename test-fn5 ".txt"))

(test-equal "Popping from a file name"
  test-fn-string1-file (pop test-fn1))

(let* ((tmp-fn (deep-clone test-fn1))
       (foo (pop! tmp-fn)))
  (test-equal "Destructive popping from a file name -- popped part is returned"
    test-fn-string1-file foo)
  (test-eq "Destructive popping from a file name -- original file
name is modified"
    (1- (length (route test-fn1))) (length (route tmp-fn))))

(test-eq "Pushing to a file name"
  3
  (string-suffix-length (file-name->string (push test-fn1 "deg"))
                        "deg"))

(let* ((tmp-fn (deep-clone test-fn1)))
  (push! tmp-fn "deg")
  (test-eq "Destructive pushing to a file name -- original file
name is modified"
    3 (string-suffix-length (file-name->string tmp-fn) "deg")))

(test-equal "Finding a common prefix"
  test-fn-string1-dir
  (file-name->string (common-prefix test-fn1 test-fn5)))

(test-equal "Finding a common prefix -- relative paths"
  test-fn-string2-dir
  (file-name->string (common-prefix test-fn2 test-fn6)))

(test-eq "No common prefix between an absolute file name and a relative one"
  #f (route (common-prefix test-fn1 test-fn2)))

(test-assert "Test if one file name is the prefix of another"
  (prefix? test-fn1-dir test-fn1))

(test-assert "Removing a prefix results in a relative file name"
  (not (fn-absolute-file-name? (remove-prefix test-fn1 test-fn1-dir))))

(test-assert "Removing a prefix"
  (file-name=? test-fn1-file
               (remove-prefix test-fn1 test-fn1-dir)))

(let ((tmp-fn (deep-clone test-fn1)))
  (remove-prefix! tmp-fn test-fn1-dir)
  (test-assert "Destructively removing a prefix"
    (file-name=? test-fn1-file tmp-fn)))

(test-assert "Trying to remove a prefix that's not a prefix returns #f"
  (not (remove-prefix test-fn1 test-fn2)))

(test-assert "Test if one file name is not the prefix of another"
  (not (prefix? test-fn1 test-fn5)))

(let ((tmp-fn (string->file-name test-fn-string1)))
  (test-assert "Checking file-name equality"
    (file-name=? tmp-fn test-fn1)))

(let ((tmp-fn (string->file-name test-fn-string2)))
  (test-assert "Checking relative file-name equality"
    (file-name=? tmp-fn test-fn2)))

 (let ((tmp-fn (string->file-name test-fn-string1)))
  (slot-set! tmp-fn 'cs (not (case-sensitive? test-fn1)))
  (test-assert "Checking file-name inequality -- case sensitivity"
    (not (file-name=? tmp-fn test-fn1))))

(test-assert "Checking file-name inequality -- absolute vs relative"
  (not (file-name=? test-fn1 test-fn4)))

(test-assert "Checking file-name inequality -- route"
  (not (file-name=? test-fn2 test-fn4)))

(test-equal "Getting the file name suffix"
  ".txt" (suffix test-fn5))

(test-assert "Adding a suffix"
  (file-name=? (add-suffix test-fn1 ".txt") test-fn5))

(let ((tmp-fn (deep-clone test-fn1)))
  (add-suffix! tmp-fn ".txt")
  (test-assert "Adding a suffix destructively"
    (file-name=? tmp-fn test-fn5)))

(test-end)
