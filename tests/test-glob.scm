;; Copyright (C) 2019 Brandon Invergo <brandon@invergo.net>

;; This library is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

(define-module (test-file-names)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (file-names)
  #:use-module (file-names glob))

(test-begin "glob")

(let ((results (glob (string->file-name "tests")
                     (string->file-name "*.scm")
                     'wildcard)))
  (test-assert "wildcard globbing -- stars"
    (and (member (string->file-name "tests/test-glob.scm") results
                 file-name=?)
         (member (string->file-name "tests/test-file-names.scm") results
                 file-name=?))))

(let ((results (glob (string->file-name "tests")
                     (string->file-name "test-glo?.scm")
                     'wildcard)))
  (test-assert "wildcard globbing -- single characters"
    (and (member (string->file-name "tests/test-glob.scm") results
                 file-name=?)
         (= 1 (length results)))))

(let ((results (glob (string->file-name ".")
                     (string->file-name "**/*.scm")
                     'wildcard)))
  (test-assert "wildcard globbing -- double-star"
    (and (member (string->file-name "./tests/test-glob.scm") results
                 file-name=?)
         (member (string->file-name "./tests/test-file-names.scm") results
                 file-name=?)
         (member (string->file-name "./src/file-names.scm") results
                 file-name=?)
         (member (string->file-name "./src/file-names/glob.scm") results
                 file-name=?))))

(let ((results (glob (string->file-name "tests")
                     (string->file-name ".*\\.scm")
                     'regexp)))
  (test-assert "regexp globbing"
    (and (member (string->file-name "tests/test-glob.scm") results
                 file-name=?)
         (member (string->file-name "tests/test-file-names.scm") results
                 file-name=?))))

(test-end)
